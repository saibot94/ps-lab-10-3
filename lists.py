def hello_func():
    return "hello world"

class LinkedList:
    def __init__(self):
        self.head = None
    
    def add(self, value):
        """
        This function adds a new value to the head of the list
        """
        new_node = Node(value, self.head)
        self.head = new_node

    def find(self, value):
        """
        Find a specified value in this list
        """
        if self.head == None:
            return False
        temp = self.head
        while temp is not None:
            if temp.value == value:
                return True
            temp = temp.next_node
        return False

        









class Node:
    def __init__(self, value, next_node):
        self.value = value
        self.next_node = next_node

