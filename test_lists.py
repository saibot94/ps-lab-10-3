from lists import hello_func, LinkedList, Node

def test_hello():
    assert hello_func() == "hello world"

def test_init_list():
    l = LinkedList()
    assert l.head == None

def test_init_node():
    n = Node(12, None)
    assert n.value == 12
    assert n.next_node == None

    n2 = Node(13, n)
    assert n2.value == 13
    assert n2.next_node == n

# addititon tests
def test_add_to_empty_list():
    l = LinkedList()
    l.add(13)
    assert l.head.value == 13

def test_add_two_items_in_list():
    l = LinkedList()
    l.add(13)
    l.add(14)
    assert l.head.value == 14
    assert l.head.next_node.value == 13

# find tests
"""
1. Find in an empty list
2. Find in a list with one element - positive case (found)
3. Find in a list with one element - negative case (not found)
4. Find in a list with 2 elements - positive
5. Find in a list with 2 elements - negative
"""

def test_find_empty_list():
    l = LinkedList()
    assert l.find(123) == False
    
def test_find_one_element_list_positive():
    l = LinkedList()
    l.add(12)
    assert l.find(12) == True
    
def test_find_one_element_list_negative():
    l = LinkedList()
    l.add(12)
    assert l.find(123) == False
    
def test_find_two_element_list_positive():
    l = LinkedList()
    l.add(12)
    l.add(13)
    assert l.find(12) == True
    
def test_find_two_element_list_negative():
    l = LinkedList()
    l.add(12)
    l.add(13)
    assert l.find(123) == False